from kafka import KafkaProducer
import multiprocessing


producer = KafkaProducer(bootstrap_servers='172.18.45.193:9092')
# for _ in range(1000):
def func():
    for _ in range(100):
    # for _ in range(100_000):
        producer.send('test1', b'The government in Cuba is legalising the ownership of small and medium-sized businesses This represents a monumental shift in policy from the communist-ruled country State-owned companies have traditionally been the norm in Cuba following the revolution in that brought Fidel Castro to power He nationalised Cuba\'s industries to put them into state hands The new policy allows entrepreneurs to operate businesses that have up to employees The change of heart from the government came after a month of street protests Thousands of Cubans demonstrated against dire economic conditions People also protested about a lack of food and the handling of the coronavirus pandemic').get()
        # producer.send('test1', b'hello').get()


def run():
    for _ in range(10):
        multiprocessing.Process(target=func).start()


if __name__ == '__main__':
    run()