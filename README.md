# Praca magisterska - Systemy Przetwarzania Strumieniowego

W repozytorium znajduję się cześć praktyczna pracy dyplomowej.
Aplikacja służy do pokazania funkcjonalności takiego systemu poprzez integracje z zwenętrznym serwisem  [Speech-to-Text](https://cloud.google.com/speech-to-text/?utm_source=google&utm_medium=cpc&utm_campaign=emea-pl-all-en-dr-bkws-all-all-trial-e-gcp-1010042&utm_content=text-ad-none-any-DEV_c-CRE_506512725296-ADGP_Hybrid%20%7C%20BKWS%20-%20EXA%20%7C%20Txt%20~%20AI%20%26%20ML%20~%20Speech-to-Text%23v20-KWID_43700060411698958-kwd-475108782129-userloc_9067397&utm_term=KW_google%20speech%20to%20text-NET_g-PLAC_&gclid=CjwKCAjw2bmLBhBREiwAZ6ugo7oiaIp5ju1IHdngLt2DBPESD_XuZL_Y9VqIHFgS2pKrOihvdjasFRoCDhAQAvD_BwE&gclsrc=aw.ds)  oraz do przeprowadzenia testów wydajnościowych w zależności od ilości i wielkości zdarzeń wejściowych.
Do komunikacji pomiędzy komponentami aplikacji służy [Apache Kafka](https://kafka.apache.org/).

Aplikacja jest zbudowana w architekturze mikroserwisowej, gdzie poszczególne komponenty odpowiadają za:  

1. *google-client* - jest to serwis integrujący się z zewnętrzna usługa googla [Speech-to-Text](https://cloud.google.com/speech-to-text/?utm_source=google&utm_medium=cpc&utm_campaign=emea-pl-all-en-dr-bkws-all-all-trial-e-gcp-1010042&utm_content=text-ad-none-any-DEV_c-CRE_506512725296-ADGP_Hybrid%20%7C%20BKWS%20-%20EXA%20%7C%20Txt%20~%20AI%20%26%20ML%20~%20Speech-to-Text%23v20-KWID_43700060411698958-kwd-475108782129-userloc_9067397&utm_term=KW_google%20speech%20to%20text-NET_g-PLAC_&gclid=CjwKCAjw2bmLBhBREiwAZ6ugo7oiaIp5ju1IHdngLt2DBPESD_XuZL_Y9VqIHFgS2pKrOihvdjasFRoCDhAQAvD_BwE&gclsrc=aw.ds), do uruchomienia modułu wymagane jest przeprowadzenia instalacji odpowiednich kroków zawartych w [dokumentacji](https://cloud.google.com/speech-to-text/docs/before-you-begin) (utworzenie projektu, utworzenie odpowiedniej roli, wygenerowania danych do połączenia oraz konfiguracja systemu poprzez ustawienie odpowiedniej zmiennej środowiskowej)  

2. *stream-platform* - uruchomienie silnika przetwarzania strumieniowego, stworzenie modeli przetwarzania oraz przeprowadzenia testów wydajnościowych.  

3. *test-generator* - wygenerowanie testowych danych na topic kafkowy.  

Do kompilacji danych modułów javowych wymagane jest Java 11 oraz [maven](https://maven.apache.org/).
Kompilacja polega na wykonaniu w głównym folderze projektu komendy
`mvn clean package`.
Uruchomienie modułów obydwa się standardowo poprzez komendę `java -jar {nazwa modułu}` np.
`D:\Programy\java\bin\java.exe -jar .\google-client-0.0.1-SNAPSHOT.jar`

Do poprawnego działania aplikacji musi zostać odpowiednio skonfigurowane połączenie do kafki poprzez ustawienie opowiednich parametrów (**spring.kafka.producer.bootstrap-servers** oraz **spring.kafka.template.default-topic**) w *application.properties* uruchomianych modułów.
Wybór interesującego modelu przetwarzania ustawiamy również poprzz konfiguracje w tym pliku. W celu uruchomiania odpowiedniego modelu należy ustawić odpowiednie flagi jak np. `model1.enabled=true`.
W przypadku uruchomienia testu wydajnościowego ustawiamy listę następujących flag:
```
test.size=10000
test.payload=test
benchmark.enabled=true
```
gdzie definiujemy ilość zdarzeń na sekunde oraz wielkość pojedynczego testowego komunikatu.