package pl.roman.michal.streamplatform;

import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;
import com.hazelcast.spring.context.SpringManagedContext;
import java.util.Properties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

  @Bean
  public SpringManagedContext springManagedContext() {
    return new SpringManagedContext();
  }

  @Bean
  public JetInstance instance(SpringManagedContext springManagedContext) {
    // You can configure Hazelcast Jet instance programmatically
    JetConfig jetConfig = new JetConfig()
        // configure SpringManagedContext for @SpringAware
        .configureHazelcast(hzConfig ->
            hzConfig.setManagedContext(springManagedContext));
    jetConfig.getHazelcastConfig().setClusterName("dev");
    return Jet.newJetInstance(jetConfig);
  }

  @Bean(name = "kafka_consumer_prop")
  public Properties kafkaConsumerProp(KafkaProperties kafkaProperties) {
    var properties = new Properties();
    properties.putAll(kafkaProperties.getConsumer().buildProperties());
    return properties;
  }

}
