package pl.roman.michal.streamplatform.models;

import com.hazelcast.jet.aggregate.AggregateOperations;
import com.hazelcast.jet.kafka.KafkaSources;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.WindowDefinition;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnProperty(
    value="model2.enabled",
    havingValue = "true",
    matchIfMissing = true)
public class SessionWindowWordCounter implements StreamService{
  private final Properties kafkaProperties;

  public SessionWindowWordCounter(@Qualifier("kafka_consumer_prop") Properties kafkaProperties) {
    this.kafkaProperties = kafkaProperties;
  }

  @Override
  public Pipeline create() {
    Pipeline p = Pipeline.create();
    p.readFrom(KafkaSources
            .<String, String>kafka(
                kafkaProperties, "test")
        ).withIngestionTimestamps()
//Grupujemy te same wyrazy
        .groupingKey(
            Entry::getValue
        )
//Tworzymy okno sesyjne,
//1 minutowe
        .window(
            WindowDefinition.
                session(TimeUnit.MINUTES.toMillis(1)
          ))
        .aggregate(AggregateOperations.counting())
        .writeTo(Sinks.logger());

    return p;
  }
}
