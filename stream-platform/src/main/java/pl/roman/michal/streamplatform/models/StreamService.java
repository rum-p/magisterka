package pl.roman.michal.streamplatform.models;

import com.hazelcast.jet.pipeline.Pipeline;
import java.io.Serializable;

public interface StreamService extends Serializable{
  Pipeline create();
}
