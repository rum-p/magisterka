package pl.roman.michal.streamplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamPlatformApplication {

  public static void main(String[] args) {
    SpringApplication.run(StreamPlatformApplication.class, args);
  }
}
