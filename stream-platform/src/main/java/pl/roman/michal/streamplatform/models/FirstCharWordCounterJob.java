package pl.roman.michal.streamplatform.models;

import com.hazelcast.jet.Traversers;
import com.hazelcast.jet.aggregate.AggregateOperations;
import com.hazelcast.jet.kafka.KafkaSources;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.WindowDefinition;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@ConditionalOnProperty(
    value="model1.enabled",
    havingValue = "true",
    matchIfMissing = true)
public class FirstCharWordCounterJob implements StreamService {
  private final Properties kafkaProperties;

  public FirstCharWordCounterJob(@Qualifier("kafka_consumer_prop") Properties kafkaProperties) {
    this.kafkaProperties = kafkaProperties;
  }

  @Override
  public Pipeline create() {
    var pipeline = Pipeline.create();

    pipeline
        .readFrom(
            KafkaSources
              .<String, String>kafka(
                  kafkaProperties, "test1"))
        .withIngestionTimestamps()
        .map(Entry::getValue)
        .flatMap(line -> Traversers
            .traverseArray(
                line.split("\\s")))
        .window(WindowDefinition
            .tumbling(TimeUnit.SECONDS.toMillis(5)))
        .groupingKey(it -> it
            .toLowerCase()
            .substring(0, 1))
        .aggregate(AggregateOperations
            .counting())
        .map(it -> String
            .format("key %s, value %d",
                it.getKey(), it.getValue()))
        .writeTo(Sinks.logger());

    return pipeline;
  }
}
