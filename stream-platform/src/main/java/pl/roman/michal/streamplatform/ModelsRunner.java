package pl.roman.michal.streamplatform;

import com.hazelcast.jet.JetInstance;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.roman.michal.streamplatform.models.StreamService;

@Service
@RequiredArgsConstructor
public class ModelsRunner {
  private final List<? extends StreamService> services;
  private final JetInstance jetInstance;


  @PostConstruct
  public void run(){
    services.stream().map(StreamService::create).forEach(jetInstance::newJob);
  }
}
