package pl.roman.michal.streamplatform.models;

import com.hazelcast.jet.Traversers;
import com.hazelcast.jet.aggregate.AggregateOperations;
import com.hazelcast.jet.pipeline.Pipeline;
import com.hazelcast.jet.pipeline.Sinks;
import com.hazelcast.jet.pipeline.test.TestSources;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@ConditionalOnProperty(
    value="benchmark.enabled",
    havingValue = "true",
    matchIfMissing = true)
public class Benchmark implements StreamService {

  @Value("${test.size}")
  public int testSize;

  @Value("${test.payload}")
  public String testPayload;


  @Override
  public Pipeline create() {
    long size = ((long) testSize * testPayload.split("\\s").length);

    var pipeline = Pipeline.create();

    pipeline
        .readFrom(
            TestSources
                .itemStream(1000,
                    (t1,t2) -> "tetse test "))
        .withoutTimestamps()
        .flatMap(line -> Traversers
            .traverseArray(
                line.split("\\s")))
        .rollingAggregate(AggregateOperations
            .counting())
        .filter(it -> it % size == 0)
        .map(it -> String
            .format("value %d", it))
        .writeTo(Sinks.logger());

    return pipeline;
  }
}
