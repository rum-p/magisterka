//package pl.roman.michal.streamplatform.models;
//
//import com.hazelcast.function.ComparatorEx;
//import com.hazelcast.jet.pipeline.Pipeline;
//import com.hazelcast.jet.pipeline.Sinks;
//import com.hazelcast.jet.pipeline.test.TestSources;
//import java.util.Properties;
//import java.util.concurrent.TimeUnit;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Service;
//
//@Service
//public class AggregateWordCountCounter implements StreamService {
//  private final Properties kafkaProperties;
//
//  public AggregateWordCountCounter(@Qualifier("kafka_consumer_prop") Properties kafkaProperties) {
//    this.kafkaProperties = kafkaProperties;
//  }
//
//
//  @Override
//  public Pipeline create() {
//    Pipeline p = Pipeline.create();
//    var source1 = p.readFrom(
//            TestSources.items(
//                800, 1, 23, 123,
//                563, 232, 211,
//                1231212, 1, 1,
//                2, 1));
//
//    var source2 = p.readFrom(
//              TestSources.items(
//                  1, 12, 25, 784,
//                  2563, 0, 125));
//
//        source1.merge(source2)
//            .filter(integer -> integer < 100)
//            .distinct()
//            .sort(ComparatorEx.reverseOrder())
//            .map(it-> {
//              TimeUnit.SECONDS.sleep(1000);
//              return it;
//            })
//        .writeTo(Sinks.logger());
//
//    return p;
//  }
//}
