package pl.roman.michal.googleclient;

import com.google.api.gax.rpc.ClientStream;
import com.google.api.gax.rpc.ResponseObserver;
import com.google.cloud.speech.v1.*;
import com.google.protobuf.ByteString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.sound.sampled.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class SpeechToTextService {
    private final ResponseObserver<StreamingRecognizeResponse> responseObserver;
    private final TargetDataLine targetDataLine;
    private final RecognitionConfig recognitionConfig;

    public void streamingMicRecognize() {

        try (SpeechClient client = SpeechClient.create()) {

            ClientStream<StreamingRecognizeRequest> clientStream =
                    client.streamingRecognizeCallable().splitCall(responseObserver);

            StreamingRecognitionConfig streamingRecognitionConfig =
                    StreamingRecognitionConfig.newBuilder().setConfig(recognitionConfig).build();

            StreamingRecognizeRequest request = StreamingRecognizeRequest.newBuilder().setStreamingConfig(streamingRecognitionConfig).build();
            clientStream.send(request);

            targetDataLine.start();
            log.info("Start speaking");

            long startTime = System.currentTimeMillis();
            AudioInputStream audio = new AudioInputStream(targetDataLine);

            while (true) {
                long estimatedTime = System.currentTimeMillis() - startTime;
                byte[] data = new byte[6400];
                audio.read(data);
                if (estimatedTime > 10000) { // 60 seconds
                    log.info("Stop speaking.");
                    targetDataLine.stop();
                    break;
                }
                request = StreamingRecognizeRequest.newBuilder().setAudioContent(ByteString.copyFrom(data)).build();
                clientStream.send(request);
            }
        } catch (Exception e) {
           log.error("Exception during streamingMicRecognize", e);
        }

        responseObserver.onComplete();
    }
}
