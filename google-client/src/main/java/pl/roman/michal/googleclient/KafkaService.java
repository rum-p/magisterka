package pl.roman.michal.googleclient;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaService {
    private final KafkaTemplate kafkaTemplate;

    public void sendData(String... text) {
        log.warn("Sending {} to kafka topic", text);
        Arrays.stream(text).forEach(kafkaTemplate::sendDefault);
    }
}
