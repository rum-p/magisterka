package pl.roman.michal.googleclient;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class GoogleClientApplication implements CommandLineRunner {

    private final SpeechToTextService service;

    public static void main(String[] args) {
        SpringApplication.run(GoogleClientApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        service.streamingMicRecognize();
    }
}
